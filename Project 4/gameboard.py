
class GameBoard():

    WHITE = 'W'
    BLACK = 'B'
    NEUTRAL = '.'

    def __init__(self,boardsize:tuple):
        self.size_x = int(boardsize[0])
        self.size_y = int(boardsize[1])
        self.size_max = self.size_x * self.size_y
        self.game_set = set(range(1,1+self.size_max))
        self.edge_set = self._get_edge()
        self._gamestate = self._create_board()
        # Set of possible locations
        self.directions = self._cardinal()

    def convert_xy_move(self,move:tuple) -> int:
        conv = int(move[1]) + (int(move[0])-1)*(self.size_x)
        return conv

    def set_piece(self,loc:int,color:str) -> None:
        ''' Places pieces in the gameboard string'''
        self._gamestate[loc-1] = color

    def string(self) -> str:
        ''' Return board as a string '''
        board_str = ""
        for row in range(len(self._gamestate)):
            board_str += self._gamestate[row] + " "
            # Only print a newline for intermediate rows
            if (row+1) % self.size_x == 0 and row < len(self._gamestate)-self.size_x:
                board_str += "\n"
        return board_str



    def _get_edge(self) -> set:
        ''' Determines where the edges of the board are located '''
        temp_set = set(range(1,1+self.size_x))\
                | set(range(self.size_max,self.size_max - self.size_x,-1))\
                | set(range(1,1+self.size_max,self.size_x))\
                | set(range(self.size_x,1+self.size_max,self.size_x))
        return temp_set


    def _create_board(self) -> list:
        ''' Initializes an empty board '''
        state = [self.NEUTRAL] * self.size_max
        return state 


    def _cardinal(self) -> dict:
        ''' returns the int values for navigating in the 8 cardinal directions ''' 
        left = self.size_x - 1
        right = self.size_x + 1
        return {'N':-1*self.size_x,'S':self.size_x,'W':-1,'E':+1,'SW':left,'SE':right,'NW':-1*right,'NE':-1*left}
                
