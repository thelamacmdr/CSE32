import tkinter as tk
import gui_widgets as gui
import gui_canvas as gui_canvas
from othello import Othello
from player import Player

class GuiGame:
    ''' Main GUI object '''

    def __init__(self):
        ''' Initialize window and gui '''
        self._window = tk.Tk()
        self._window.rowconfigure(0,weight=1)
        self._window.columnconfigure(1,weight=1)
        self._plugin = None # The attached widget
        self._valid = None  # Iterator for the game loop
        self.game = None    # Hold the Othello object
        self.new_game()     # Create a new game
        self.start()        # Start the mainloop

    def new_game(self):
        ''' Creates a new game '''
        self._attach_widget(gui.PromptWidget,self._config_game)

    def play_move(self,event:tk.Event,loc:int):
        ''' Plays a move on the game board '''
        # The game loop expects a function to call for the next move
        def move():
            return loc
        self._valid = self.game.game_loop(move)

        try:
            valid,moves = next(self._valid)
            self._set_score_board()
            if valid:
                self.canvas.update_state(self.game)
            # Check if we have a winner
            if len(moves) == 0:
                self._end_game(self.game.get_winner().color)
        except:
            # A hack to avoid the StopIterator exception on our generator
            pass

    def _end_game(self,color:str):
        ''' Determines the winner output '''
        winner = {
                Othello.WHITE:'WHITE WINS',
                Othello.BLACK:'BLACK WINS',
                None:"TIE GAME"}
        # Attach the widget to end the game and potentially start a new game
        self._attach_widget(gui.EndWidget,self.new_game)
        self._plugin.winner.set(winner[color])


    def _set_score_board(self):
        ''' Updates the status of the score board '''
        if self.game.current_player.color == 'W':
            white_score = len(self.game.current_player.moves)
            black_score = len(self.game.next_player.moves)
            current_player = "WHITE"
        else:
            white_score = len(self.game.next_player.moves)
            black_score = len(self.game.current_player.moves)
            current_player = "BLACK"

        # Set variables 
        self._plugin.white_points.set(str(white_score))
        self._plugin.black_points.set(str(black_score))
        self._plugin.current_turn.set(current_player)


    def _config_game(self):
        ''' Creates a new game using provided configs '''
        # Get config values
        color = self._plugin.starting_color.get()
        corner = self._plugin.corner_color.get()
        win = self._plugin.win_condition.get()
        x = int(self._plugin.x.get())
        y = int(self._plugin.y.get())

        # Configure Players
        one = Player(color)
        if color == Othello.BLACK:
            two = Player(Othello.WHITE)
        else:
            two = Player(Othello.BLACK)

        # Configure Othello state
        self.game = Othello((one,two),(x,y),win)
        self.game.start_game(corner)

        # Configure game canvas
        self.canvas = gui_canvas.GameCanvas(self._window,x,y,self.play_move)
        self.canvas.update_state(self.game)
        self.canvas.grid(column=1,row=0,sticky=tk.W+tk.N+tk.S+tk.E)

        # Attach game play widget
        self._attach_widget(gui.GameWidget)
        self._set_score_board()


    def _attach_widget(self,widget,callback=None):
        ''' Attaches a widget to the current window '''
        # Check if we need to clear the previous widget
        if self._plugin:
            self._plugin.grid_forget()
        # Attach and size the widget
        self._plugin = widget(self._window,callback)
        self._plugin.grid(column=0,row=0)
        self._plugin.grid_propagate(0)

    def start(self):
        ''' Begins main event loop '''
        self._window.mainloop()

if __name__ == '__main__':
    game = GuiGame()
