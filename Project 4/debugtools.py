import gameboard
from othello import Othello
import player, unittest,random,pathlib

class TestConsoleGame(unittest.TestCase):


    def setUp(self):
        one = player.Player('W')
        two = player.Player('B')
        othellogame = Othello((one,two),(4,4),'<')
        othellogame.start_game('W')


    def test_board(self):
        ''' Test individual pieces '''
        X = 8
        Y = 3
        board = gameboard.GameBoard((X,Y))
        self.assertEqual(board.game_set, set(range(1,X*Y+1)),"wrong board game set")
        self.assertEqual(board.directions['N'],-1*X,"bad direction")   
        self.assertEqual(board.directions['SE'],X +1,"bad direction")
        self.assertTrue(". . . . . . . . \n" in board.string(),"incorrect board string")
    

    def test_smoke(self):
        ''' Test entire board with Othello'''
        one = player.Player('W')
        two = player.Player('B')
        self.assertTrue(one.moves.issubset(set([6,11])),"incorrect start state")
        self.assertTrue(two.moves.issubset(set([7,10])),"incorrect start state")


    def test_win(self):
        one = player.Player('W')
        two = player.Player('B')
        for i in range(1):
            if i % 2 == 0:
                othellogame = Othello((one,two),(100,100),'>')
            else:
                othellogame = Othello((one,two),(100,100),'<')
            othellogame.start_game('B')
            for result,moves in othellogame.game_loop(self._roll_random_move):
                m = 1
            cur = len(othellogame.current_player.moves)
            nex = len(othellogame.next_player.moves)

            if othellogame.get_winner() == None:
                self.assertEqual(cur,nex,"tie game invalid")
            else:
                self.assertNotEqual(cur,nex,"no winner found in game")
            one = player.Player('W')
            two = player.Player('B')
                

    def _roll_random_move(self):
        size = 100*100
        random.seed()
        playable = random.randint(1,size)
        return playable


if __name__ == "__main__":
    unittest.main()

