
class Player():

    def __init__(self,color:str):
        self.color = color
        self.moves = set()

    def add_moves(self,loc):
        self.moves.update(loc)

    def undo_moves(self,loc):
        self.moves.difference_update(loc)
