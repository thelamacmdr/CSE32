# Anthony Lam
# 77341985

from othello import Othello
from gameboard import GameBoard
from player import Player
import collections

class Console():
    _GAME_TYPE = "FULL"
    _INPUT_TYPES = ["x","y","first_pos","p_one","win"]
    Data = collections.namedtuple("Data",_INPUT_TYPES)

    def __init__(self):
        ''' Initialize a game '''
        self.data = self._get_game_data()
        self._set_players(self.data.p_one.upper())
        self.othello = Othello((self.one,self.two),(self.data.x,self.data.y),self.data.win)
        self.othello.start_game(self.data.first_pos.upper())
        
    def _set_players(self,p_one:str) -> None:
        ''' Sets players '''
        self.one = Player(self.data.p_one)
        if self.data.p_one == Othello.BLACK:
            self.two = Player(Othello.WHITE)
        else:
            self.two = Player(Othello.BLACK)

    def _get_game_data(self) -> list:
        ''' Prints out game type and gets input for it '''
        print(self._GAME_TYPE)
        d = []
        for entry in self._INPUT_TYPES:
            d.append(input())
        game_data = self.Data(d[0],d[1],d[3],d[2],d[4])
        return game_data

    def _print_status(self) -> None:
        ''' Prints out current number of pieces per player '''
        if self.othello.current_player.color == "B":
            black = len(self.othello.current_player.moves)
            white = len(self.othello.next_player.moves)
        else:
            black = len(self.othello.next_player.moves)
            white = len(self.othello.current_player.moves)
        print("B: {}  W: {}".format(black,white))


    def output(self) -> None:
        ''' Prints out the board '''
        self._print_status()
        # Print board
        print (self.othello.board.string())

    def start_game(self) -> None:
        ''' Main game loop '''
        game = self.othello.game_loop(self._get_move)
        self.output()
        print("TURN: {}".format(self.othello.current_player.color))
        # Loop through an othello game
        for valid,moves in game:
            if valid:
                print("VALID")
                self.output()
                if moves:
                    print("TURN: {}".format(self.othello.current_player.color))
            else:
                print("INVALID")
        self.print_winner(self.othello.get_winner())


    def print_winner(self,winner:Player) -> None:
        ''' Print out a winner '''
        if winner:
            print("WINNER: {}".format(winner.color))
        else:
            print("WINNER: NONE")

    def _get_move(self) -> int:
        ''' Take input for a player move '''
        move = input().split()
        return self.othello.board.convert_xy_move(move)
        
if __name__ == "__main__":
    new_game = Console()
    new_game.start_game()
