from gameboard import GameBoard
from player import Player

class Othello():

    WHITE = GameBoard.WHITE
    BLACK = GameBoard.BLACK

    def __init__(self,players:tuple,boardsize:tuple,win:str):
        self.current_player = players[0]
        self.next_player = players[1]
        self.condition = win
        self.board = GameBoard(boardsize)

    
    def start_game(self,color:str) -> None:
        ''' Sets starting pieces '''
        # Determine the starting location for the "center" of the board
        start = int((self.board.size_x / 2)*(self.board.size_y -1))
        if self.current_player.color == color:
            self.current_player.add_moves([start,start+self.board.size_x+1])
            self.next_player.add_moves([start+1,start+self.board.size_x])
        else:
            self.next_player.add_moves([start,start+self.board.size_x+1])
            self.current_player.add_moves([start+1,start+self.board.size_x])
        self.update()
            
    def update(self) -> None:
        ''' Applies move changes to the gameboard '''
        for move in self.current_player.moves:
            self.board.set_piece(move,self.current_player.color)

        for move_2 in self.next_player.moves:
            self.board.set_piece(move_2,self.next_player.color)

            
    def get_winner(self) -> Player:
        ''' Gets the winner of the game, returns None if tied '''
        greater = None
        lesser = None
        if len(self.current_player.moves) > len(self.next_player.moves):
            greater = self.current_player
            lesser = self.next_player
        elif len(self.current_player.moves) < len(self.next_player.moves):
            greater = self.next_player
            lesser = self.current_player

        if self.condition == "<":
            return lesser
        else:
            return greater
            
    def play_move(self,loc:int,available_moves:dict) -> bool:
        ''' Places a piece on the board, if valid '''
        if loc in available_moves:
            # Swap pieces from player one set to player two set
            # AKA: FLIP pieces
            move = available_moves[loc]
            move.add(loc)
            self.current_player.add_moves(move)
            self.next_player.undo_moves(move)
            return True
        return False


    def game_loop(self,input_func:"give player move input") -> bool:
        ''' Generator providing a boolean for valid/invalid moves '''
        available_moves = self._valid_moves()
        fail_count = 0
        while fail_count < 2 and available_moves:
            is_valid_move = False
            # Play a move if it's valid
            if self.play_move(input_func(),available_moves):
                is_valid_move = True
                self.update()
                self._swap_players()
                available_moves = self._valid_moves()
                # Check if there are available moves left for the current plyaer
                if not available_moves:
                    # Swap players and let them move
                    self._swap_players()
                    available_moves = self._valid_moves()
                    # Failing twice means no one has any moves left
                    fail_count += 1
                else:
                    fail_count = 0
            # Return whether this is a valid move, and the number of valid moves left
            yield is_valid_move,available_moves



    def _cardinal_search(self) -> dict:
        ''' Applies flip search in 8 cardinal dirs '''
        pieces = {}
        for loc in self.current_player.moves:
            search = self._flip_search(loc)
            for key,value in search.items():
                pieces.setdefault(key,set()).update(value)
        return pieces


    def _flip_search(self,loc:int) -> dict:
        ''' Searches for pieces to flip, this does not check for invalid moves '''
        flippable = {}
        # Search in the specified direction
        for direction,value in self.board.directions.items():
            path = set()
            new_loc = loc + value
            at_edge = False
            # Continue navigating while the piece belongs to another player
            while new_loc in self.next_player.moves :
                path.add(new_loc)
                new_loc += value

            # Don't add the path if it ends past an edge
            if new_loc not in self.board.game_set:
                at_edge = True
            elif direction not in {'S','N'}:
                temp = (new_loc - value) % self.board.size_x
                if temp == 0 or temp == 1:
                    at_edge = True

            # Add to our list of possible moves if a path was found
            if path and not at_edge:
                flippable[new_loc] = path
        return flippable



    def _valid_moves(self) -> dict:
        ''' Return the valid locations a player can move '''
        available_moves = self._cardinal_search()
        # Obtain a set of valid moves
        playable = self.board.game_set - (self.current_player.moves | self.next_player.moves)
        for i in list(available_moves):
            if i not in playable:
                available_moves.pop(i)
        return available_moves


    def _swap_players(self):
        ''' Swaps current player to next player '''
        temp = self.current_player
        self.current_player = self.next_player
        self.next_player = temp

