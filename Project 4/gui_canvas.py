import tkinter as tk
from player import Player
from othello import Othello

class GameCanvas(tk.Canvas):

    _BLACK = "#000"
    _WHITE = "#FFF"

    def __init__(self, window:tk.Tk, cols:int, rows:int,on_click:object,*args,**kwargs):
        tk.Canvas.__init__(self,master=window,width=500,height=500,*args,**kwargs)
        self.bind('<Configure>',self.redraw)
        self._on_click = on_click
        self._row = rows
        self._col = cols
        self._game = None

    def redraw(self,event:tk.Event):
        ''' Clear and redraw the gameboard '''
        self.delete(tk.ALL)
        self.partition_col = self.winfo_width()/self._col
        self.partition_row = self.winfo_height()/self._row

        # Get the playable locations
        self._plays = self._playables()
        # Bind a click event to playable rectangles
        self._click_bind(self._plays)
        if self._game:
            self.draw_pieces(self._game.current_player)
            self.draw_pieces(self._game.next_player)


    def update_state(self,game:Othello):
        ''' Updates the gameboard based on the Othello gameboard '''
        self._game = game
        self.redraw(None)


    def draw_pieces(self,player:Player) -> None:
        ''' Helper method to draw a set of pieces '''
        w = self._BLACK
        if player.color == Othello.WHITE:
            w = self._WHITE

        for move in player.moves:
            self._draw_game_piece(w,self._plays[move],)


    def _click_bind(self,loc:dict) -> None:
        ''' Binds button click events to all boxes of the game board '''
        for key,frac in loc.items():
            id = self.create_rectangle(frac[0],frac[1],frac[2],frac[3],tags=str(key),fill="#00cc66")

            def click_handler(event,self=self,key=key):
                return self._on_click(event,key)

            self.tag_bind(id,'<ButtonPress-1>',click_handler)


    def _playables(self) -> dict:
        ''' Get a dictionary of piece locations for the canvas '''
        boxes = {}
        pos = 1
        for bound_y in range(0,self._row):
            for bound_x in range(0,self._col):
                loc_x = bound_x * self.partition_col
                loc_y = bound_y * self.partition_row
                # A tuple representation of the bounding box for an oval to draw
                boxes[pos] =  (loc_x,loc_y,loc_x+self.partition_col,loc_y+self.partition_row)
                pos += 1
        return boxes


    def _draw_game_piece(self,color:str,frac:tuple)->None:
        padding = 5
        ''' Draws a game piece with fractional coordinates '''
        self.create_oval(frac[0]+padding,frac[1]+padding,frac[2]-padding,frac[3]-padding,fill=color)
