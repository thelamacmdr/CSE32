import tkinter as tk
from player import Player
from othello import Othello

class OthelloWidget(tk.LabelFrame):

    def __init__(self,window,on_click:object,*args,**kwargs):
        ''' Configure widget base '''
        self.initial_config()
        # Defines an overall submit callback
        self._submit = on_click
        tk.LabelFrame.__init__(self,window,text=self.label,height=500,width=300,*args,**kwargs)
        # Configure a two column widget
        self.columnconfigure(0,weight=1,minsize=150)
        self.columnconfigure(1,weight=1,minsize=150)
        self.build_widget()

    def initial_config(self):
        ''' Configuration before creating the frame '''
        pass

    def build_widget(self):
        ''' Configuration after creating the frame '''
        pass


class PromptWidget(OthelloWidget):
    ''' UI widget for starting a game '''

    def initial_config(self):
        self.label = "CONFIGURE GAME"
        self.x = tk.StringVar()
        self.y = tk.StringVar()
        self.starting_color = tk.StringVar()
        self.corner_color = tk.StringVar()
        self.win_condition = tk.StringVar()

    def build_widget(self):
        # Widget creation
        label_x = tk.Label(self,text="Columns")
        label_y = tk.Label(self,text="Rows")
        label_start_player = tk.Label(self,text="Starting Color")
        label_start_corner = tk.Label(self,text="Corner Color")
        label_win_condition = tk.Label(self,text="Win Condition")

        size_select_x = tk.Spinbox(self,from_=4,to=16,increment=2,textvariable=self.x)
        size_select_y = tk.Spinbox(self,from_=4,to=16,increment=2,textvariable=self.y)

        rb_start_black = tk.Radiobutton(self,variable=self.starting_color,value="B",text="Black")
        rb_start_white = tk.Radiobutton(self,variable=self.starting_color,value="W",text="White")
        self.starting_color.set('W')

        rb_corner_black = tk.Radiobutton(self,variable=self.corner_color,value="B",text="Black")
        rb_corner_white = tk.Radiobutton(self,variable=self.corner_color,value="W",text="White")
        self.corner_color.set('W')

        rb_win_less = tk.Radiobutton(self,variable=self.win_condition,value="<",text="Least Pieces")
        rb_win_greater = tk.Radiobutton(self,variable=self.win_condition,value=">",text="Most Pieces")
        self.win_condition.set('<')

        submit = tk.Button(self,text="Submit",command=self._submit)

        # Grid Assignment
        label_x.grid(column=1,row=0)
        label_y.grid(column=0,row=0)
        size_select_x.grid(column=1,row=1)
        size_select_y.grid(column=0,row=1)

        label_start_player.grid(column=0,row=2)
        rb_start_black.grid(column=0,row=3)
        rb_start_white.grid(column=0,row=4)

        label_start_corner.grid(column=1,row=2)
        rb_corner_black.grid(column=1,row=3)
        rb_corner_white.grid(column=1,row=4)

        label_win_condition.grid(column=0,row=5,columnspan=2)
        rb_win_less.grid(column=0,row=6)
        rb_win_greater.grid(column=1,row=6)

        submit.grid(column=0,row=7,columnspan=2)

class GameWidget(OthelloWidget):
    ''' UI Widget during gameplay '''

    def initial_config(self):
        self.label = "FULL OTHELLO"
        self.white_points = tk.StringVar()
        self.black_points = tk.StringVar()
        self.current_turn = tk.StringVar()

    def build_widget(self):
        font = ('TkDefaultFont','18','bold')
        label_score = tk.Label(self,text="Score",font=font)
        label_white_score = tk.Label(self,text="White",font=font)
        label_black_score = tk.Label(self,text="Black",font=font)
        label_white_points = tk.Label(self,textvariable=self.white_points,font=font)
        label_black_points = tk.Label(self,textvariable=self.black_points,font=font)
        label_turn = tk.Label(self,text="Current Turn",font=font)
        label_current_turn = tk.Label(self,textvariable=self.current_turn,font=font)

        label_score.grid(column=0,row=0,columnspan=2)
        label_white_score.grid(column=0,row=1)
        label_black_score.grid(column=1,row=1)

        label_white_points.grid(column=0,row=2)
        label_black_points.grid(column=1,row=2)

        label_turn.grid(column=0,row=3,columnspan=2)
        label_current_turn.grid(column=0,row=4,columnspan=2)

class EndWidget(OthelloWidget):
    ''' UI Widget after gameplay '''
     
    def initial_config(self):
        self.label = ""
        self.winner = tk.StringVar()

    def build_widget(self):
        font = ('TkDefaultFont','20','italic')
        label_message = tk.Label(self,text="Game Over",font=font)
        label_winner = tk.Label(self,textvariable=self.winner)
        button_play_again = tk.Button(self,text="Play Again",command=self._submit)

        label_message.grid(column=0,row=0,columnspan=2)
        label_winner.grid(column=0,row=1,columnspan=2)
        button_play_again.grid(column=0,row=2,columnspan=2)
                

