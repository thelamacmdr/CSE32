# Project 1
# Anthony Lam - 77341985
# Jonathan Young - 74575631

from pathlib import Path
import shutil


### INPUT FUNCTIONS

def get_input_path() -> Path: 
    ''' Takes in input and returns a Path'''
    while True:
        try:
            input_path = input()
            if input_path:
                path =  Path(input_path)
                return path.resolve()
            else:
                print("ERROR")
        except:
            print("ERROR")

def get_input_search(contents:list) -> list:
    ''' Takes in splitted input and returns list of file'''
    while True:
        try:
            inputlist = input().split(None,1)
            first_input = inputlist[0].lower()
            second_input = inputlist[1]

            cur_function = None

            if first_input == "n":
                cur_function = search_filename
                break
            elif first_input == "e":
                cur_function = search_extension
                break
            elif first_input == "s":
                second_input = int(second_input)
                cur_function = search_size
                break
            else:
                print("ERROR")
        except:
            print("ERROR")
            
    return iter_list(contents,cur_function,second_input)

def get_input_action(contents:list) -> None:
    ''' Takes a list of files and executes a specified action'''
    while True:
        try:
            inputlist = input().split()
            first_input = inputlist[0].lower()
            cur_function = None
            
            if first_input == "p":
                cur_function = action_print
                break
            elif first_input == "f":
                cur_function = action_readline
                break
            elif first_input == "d":
                cur_function = action_duplicate
                break
            elif first_input == "t":
                cur_function = action_touch
                break
            else:
                print("ERROR")
        except:
            print("ERROR")
    iter_list(contents,cur_function)

### DIRECTORY NAVIGATION FUNCTIONS

def file_walking(current_directory: Path) -> list:
    ''' Traverses subdirectories in a given Path and outputs a list of files '''
    list_files = []
    
    for contents in current_directory.iterdir():
        try:
            if contents.is_dir():
                list_files = list_files + file_walking(contents)
            elif contents.is_file():
                list_files.append(contents)
        except:
            pass
    return list_files
    
### SEARCH FUNCTIONS

def search_filename(item:Path, filename:str) -> list:
    ''' Takes in a list of files and returns list with filename'''
    if item.name == filename:
        return item

def search_extension(item:Path, extension_name: str) -> list:
    ''' Takes in a Path and checks for a valid extension '''
    if item.suffix == extension_name or item.suffix == "."+extension_name:
        return item

def search_size(item:Path, file_size: int) -> list:
    ''' Checks for a given file size (bytes)'''
    if item.stat().st_size > file_size:
        return item


### ACTION FUNCTIONS

def action_print(item:Path) -> None:
    ''' Returns the current working file'''
    print(str(item))

def action_readline(item:Path)-> None:
    ''' Outputs first line of each file'''
    action_print(item)
    with item.open() as f:
        print(f.readline(),end="")
    f.close()
    
def action_duplicate(item:Path)-> None:
    ''' Duplicates a file and appends .dup'''
    action_print(item)
    shutil.copy(str(item),str(item) + ".dup")

def action_touch(item:Path)-> None:
    ''' Processes a touch on a file'''
    action_print(item)
    item.touch()

### HELPER FUNCTIONS

# Has a default value for function arguments
# Note: None has a truthiness of False, all other vals are True
def iter_list(list_files:list,function,args=None) -> list:
    ''' Executes a function on a list of paths'''
    path_list = []
    value = None
    for item in list_files:
        if args:
            value = function(item,args)
        else:
            value = function(item)
        if value:
            path_list.append(value)
    return path_list

def main()-> None:
    ''' Main entry function'''
    # Get a directory from the user
    cwd = get_input_path()
    # Obtain a list of files from the given directory
    list_files = file_walking(cwd)

    search_value = get_input_search(list_files)
    action_value = get_input_action(search_value)

if __name__ == '__main__':
    main()










# Project 1
# Anthony Lam
# Jonathan Young

from pathlib import Path
import shutil


#### INPUT FUNCTIONS


# Obtains and parses input
def get_input_path() -> Path: 
    ''' Takes in input and returns a Path'''
    while True:
        try:
            input_path = input()
            path =  Path(input_path)
            return path.resolve()
        except FileNotFoundError:
            print("ERROR")
        except:
            print("ERROR")

def get_input_search(contents: list) -> list:
    ''' Takes in splitted input and returns list of file'''
    while True:
        try:       
            inputlist = input().split(None, 1)
            first_input = inputlist[0].lower()
            second_input = inputlist[1]

            if first_input == "n":
                return search_filename(contents, second_input)
            elif first_input == "e":
                return search_extension(contents, second_input)
            elif first_input == "s":
                return search_size(contents, int(second_input))
            else:
                print("ERROR")

        except:
            print("ERROR")



def get_input_action(contents: list) -> None:
    ''' Takes in splitted input and performs an action on files'''
    while True:
        try:
	first_input = input().lower()
            if len(first_input) > 1:
                print("Error")

            if first_input == "p":    
                directory_print(contents)
                break
            elif first_input == "f":
                action_readline(contents)
                break
            elif first_input == "d":
                action_duplicate(contents)
                break
            elif first_input == "t":
                action_touch(contents)
                break
            else:
                print("ERROR")
                
        except:
            print("ERROR")



def file_walking(current_directory: Path) -> list:
    ''' Traverses directories and files and outputs a list of files '''
    list_files = []

    for contents in current_directory.iterdir():    
        try:
            if contents.is_dir():
                list_files = list_files + file_walking(contents)
            elif contents.is_file():
                list_files.append(contents)
        except:
            print("ERROR")
    return list_files


#### SEARCH FUNCTIONS

def search_filename(list_files:list, filename:str) -> list:
    ''' Takes in a list of files and returns list with filename'''
    list_found_files = []

    for item in list_files:
        if item.name == filename:
            list_found_files.append(item)
    return list_found_files

def search_extension(list_files: list, extension_name: str) -> list:
    ''' Takes in a list of files and returns a list of files with same extension'''
    exten_list = []
    
    for item in list_files:
        if item.suffix == extension_name or item.suffix == "." + extension_name:
            exten_list.append(item)
    return exten_list

def search_size(list_files: list, file_size: int) -> list:
    ''' Takes in a list of files and retuns a list of files with
        memory > file_size '''
    result = []

    for item in list_files:
        if item.stat().st_size > file_size:
            result.append(item)
    return result


#### ACTION FUNCTIONS


def action_print(pathname: Path) -> None:
    ''' Prints out the the Path'''
    print(str(pathname))

def directory_print(contents: list) -> None:
    ''' Goes through a list of paths and prints out the Path'''
    for item in contents:
        action_print(item)

def action_readline(list_files: list) -> None:
    ''' Outputs first line of each file'''
    for item in list_files:
        try:
            action_print(item)
            with item.open() as f:
                print(f.readline(), end = "")
        except:
            pass
    f.close()

def action_duplicate(list_files: list) -> None:
    ''' Duplicates the file and adds .dup to end of file'''
    for item in list_files:
        action_print(item)
        shutil.copy(str(item), str(item) + ".dup")

def action_touch(list_files: list) -> None:
    ''' Touches file and changes timestamp'''
    for item in list_files:
        action_print(item)
        item.touch()


#### MAIN FUNCTION

def main():
    ''' Main function'''
    cwd = get_input_path()
    list_files = file_walking(cwd)
    search_value = get_input_search(list_files)
    action_value = get_input_action(search_value)


if __name__ == '__main__':
    main()
