# Project 2 - Connect Four
# Anthony Lam

## Deliverables
* Local version of game
* Networked game
* connectfour.py
* Common functions file


## Display format
1 2 3 4 5 6 7
. . . . . . .
. . . . . . .
. . . . . . .
. . R . . . .
. . Y R . . .
. R R Y . Y .


## User Inputs

### Local
* Column choice

### Networked
* Username - no whitespace allowed
* Hostname/IP
* Port
* Column choice

***

## Network Protocol
eol = \r\n

### Establishing communication
1. Connect to server
2. client ENQ - "I32CFSP_HELLO username eol"
3. server ACK - "WELCOME username eol"
4. client ENQ - "AI_GAME eol"
5. server ACK - "READY eol"

### Playing the game
* Client playing : "DROP # eol" or "POP # eol"
    * Server responds: "OKAY eol"
* Winner: "WINNER_RED eol" or "WINNER_YELLOW eol"
* Invalid move: "INVALID eol"
* Server playing : "DROP # eol READY" or "POP # eol READY"
