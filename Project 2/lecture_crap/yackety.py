# yackety.py
#
# ICS 32 Fall 2015
# Code Example
#
# This module provides a set of functions that implement the Yackety protocol
# using sockets, allowing a Python program to connect to a Yackety server and
# use it to send and view Yackety messages, while insulating that program
# from the underlying details of how the protocol is implemented.
#
# It's important to note that the module contains no user interface and is
# not a "program" that can be executed.  Instead, it provides utility
# functions that can be used by programs, in the same way that modules like
# "os" and "socket" do in the Python Standard Library.  It's fair to say,
# actually, that this module is a small library.  (See?  We can build our
# own libraries, too!)

from collections import namedtuple
import socket



# From our work with sockets in previous examples, we discovered that we
# needed to know three things about a connection at any given time:
#
# (1) The socket across which the connection is traveling
# (2) A pseudo-file object that lets us read input from that socket as
#     though we were reading from a text file
# (3) A pseudo-file object that lets us write input to that socket as
#     though we were writing to a text file
#
# Because these three things need to be available to various functions
# in our module, it's handy to create a kind of object to store all three,
# so we only have one thing to store, one thing to return, and one thing
# to pass as a parameter.  A namedtuple is a convenient way to do that.

YacketyConnection = namedtuple(
    'YacketyConnection',
    ['socket', 'input', 'output'])



# When we ask the Yackety server to send us the last N messages, it will
# be handy to return them in a way that separates the username from the
# content of the message.  For that reason, we'll create a kind of object
# -- another namedtuple -- to store each message.

YacketyMessage = namedtuple(
    'YacketyMessage',
    ['username', 'text'])



# This is the simplest example of how you create new kinds of exceptions
# that are specific to your own program.  A class introduces a new type of
# object into your program.  In this case, we're introducing a new type called
# YacketyError, and specifying that we want it to be a kind of exception (i.e.,
# something that we can raise to indicate failure).

class YacketyError(Exception):
    pass



def connect(host: str, port: int) -> YacketyConnection:
    '''
    Connects to a Yackety server running on the given host and listening
    on the given port, returning a YacketyConnection object describing
    that connection if successful, or raising an exception if the attempt
    to connect fails.
    '''

    yackety_socket = socket.socket()
    
    yackety_socket.connect((host, port))

    yackety_input = yackety_socket.makefile('r')
    yackety_output = yackety_socket.makefile('w')

    return YacketyConnection(
        socket = yackety_socket,
        input = yackety_input,
        output = yackety_output)



def hello(connection: YacketyConnection, username: str) -> None:
    '''
    Logs a user into the Yackety service over a previously-made connection.
    The username is to be provided without an @ symbol, to make it impossible
    for callers to provide an invalid username.  If the attempt to send text
    to Yackety or receive a response fails (or if the server sends back a
    response that does not conform to the Yackety protocol), an exception is
    raised.
    '''

    # The _write_line and _expect_line functions are written below.  Their
    # goal is to hide the details of interacting with the socket (e.g.,
    # putting the correct newline sequence on the end of the line, remembering
    # to flush the output after writing it, etc.).  Notice how, given those
    # tools, the code we've written here is terse and clear, relative to
    # what it would look like if we had these details interspersed throughout
    # this function.
    _write_line(connection, 'YACKETY_HELLO @' + username)
    _expect_line(connection, 'YACKETY_HELLO')



def send(connection: YacketyConnection, message: str) -> None:
    '''
    Sends a message to the Yackety server on behalf of the currently-
    logged-in user.  If the communication fails (or the server sends
    back a response that does not conform to the Yackety protocol), an
    exception is raised.
    '''

    _write_line(connection, 'YACKETY_SEND ' + message)
    _expect_line(connection, 'YACKETY_SENT')



def last(connection: YacketyConnection, how_many_messages: int) -> [YacketyMessage]:
    '''
    Retrieves the most recent few messages from Yackety.  The how_many_messages
    parameter determines how many messages we want; the Yackety server will
    send back as many as it has, up to the number we asked for.  The result
    of this function is a list of YacketyMessage objects, one per message
    sent back from the server, in the reverse of the order they were originally
    sent to Yackety (i.e., newest message first).  If the communication fails (or
    the server sends back a response that does not conform to the Yackety protocol),
    an exception is raised.
    '''

    _write_line(connection, 'YACKETY_LAST {}'.format(how_many_messages))

    messages = []

    # The Yackety protocol responds to the "YACKETY_LAST x" message
    # by sending, first, a count of how many messages it is responding
    # with.  This is done by sending a line "YACKETY_MESSAGE_COUNT y".
    # We need to know what the number y is.
    message_count_line = _read_line(connection)

    if message_count_line.startswith('YACKETY_MESSAGE_COUNT '):
        # We'll look at the characters on the line starting with index 22,
        # which skips "YACKETY_MESSAGE_COUNT " and convert those characters
        # to an integer.
        number_of_messages = int(message_count_line[22:])

        for i in range(number_of_messages):
            message_line = _read_line(connection)

            # To understand each message, we'll need to carefully break up
            # the line that was sent:
            #
            # * The first word should be YACKETY_MESSAGE
            # * The second word should be a username
            # * After the second word should be a space and then the full
            #   contents of the message, which we'll want to preserve
            #   including all spaces, punctuation, etc.
            if message_line.startswith('YACKETY_MESSAGE'):
                # Break the message into words, so we can pull out the username
                message_words = message_line.split()

                # The username is the second word
                username = message_words[1]

                # The text of the message starts after "YACKETY_MESSAGE", a
                # space, the username, and another space.  "YACKETY_MESSAGE"
                # and the two spaces are 17 characters total; the username's
                # length will vary depending on the message.  But if we add
                # 17 and the length of the username, that will reliably tell
                # us where the message text starts.
                text_start = 17 + len(username)

                # Pull out the message text
                text = message_line[text_start:]

                # Create a YacketyMessage object and append it to a list of
                # messages that we plan to return
                messages.append(YacketyMessage(username, text))

    # Return all of the messages that came back from the server
    return messages



def goodbye(connection: YacketyConnection) -> None:
    'Exchanges YACKETY_GOODBYE messages with the server'

    _write_line(connection, 'YACKETY_GOODBYE')
    _expect_line(connection, 'YACKETY_GOODBYE')



def close(connection: YacketyConnection) -> None:
    'Closes the connection to the Yackety server'

    # To close the connection, we'll need to close the two pseudo-file
    # objects and the socket object.
    connection.input.close()
    connection.output.close()
    connection.socket.close()



# These are "private functions", by which I mean these are functions
# that are only intended to be used within this module.  They're
# hidden implementation details, used only to make writing other functions
# in this module easier.  By starting their names with an underscore,
# we're making clear to users of this module that these functions are
# intended to be private -- this convention is typical in Python programs,
# so if we name things beginning with underscores, it's a strong hint to
# knowledgeable Python programmers that our intent is for these things
# only to be used where they're defined (in this case, only within the
# yackety.py module).



def _read_line(connection: YacketyConnection) -> str:
    '''
    Reads a line of text sent from the server and returns it without
    a newline on the end of it
    '''

    # The [:-1] uses the slice notation to remove the last character
    # from the string.  Since we know that readline() will always
    # return a line of text with a '\n' character on the end of it,
    # the slicing here will ensure that these will always be stripped
    # out, so we'll never have to deal with this detail elsewhere.
    return connection.input.readline()[:-1]



def _expect_line(connection: YacketyConnection, expected: str) -> None:
    '''
    Reads a line of text sent from the server, expecting it to contain
    a particular text.  If the line of text received is different, this
    function raises an exception; otherwise, the function has no effect.
    '''

    line = _read_line(connection)

    if line != expected:
        raise YacketyError()



def _write_line(connection: YacketyConnection, line: str) -> None:
    '''
    Writes a line of text to the server, including the appropriate
    newline sequence, and ensures that it is sent immediately.
    '''
    connection.output.write(line + '\r\n')
    connection.output.flush()
