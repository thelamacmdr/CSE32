import socket


class Room():

    global EOL
    EOL = "\r\n"

    def __init__(self,host,port):
        self.connection = self._open_connection(host,port)

    @classmethod
    def send_message(self):
        print("Message: ")
        self.connection["output"].write(input() + EOL)
        self.connection["output"].flush()


    @classmethod
    def expect_message(self):
        print("Received: ")
        self.connection["input"].readline()

    @classmethod
    def _ack(self,connection):
        self.connection["output"].write("ack420" + EOL)


    @classmethod
    def _open_connection(self,host, port):
        mysocket = socket.socket()
        mysocket.connect((host,port))

        return { "input":mysocket.makefile('r'),
                 "output":mysocket.makefile('w')}

def main():
    cur_item = True
    room = Room("159.203.243.30",8888)

    while True:
        if cur_item:
            room.send_message()
        else:
            room.expect_message()

main()

