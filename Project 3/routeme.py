# Anthony Lam
# 77341985

import mapquest
import json, random
import generator as g

def output(choices:[str],mapdata:"MapData") -> None:
    print()
    # Possible input choices
    choiceMap = {
            "LATLONG":g.LatlngOutput(mapdata.latlng()),
            "STEPS":g.DirectionOutput(mapdata.narrative()),
            "ELEVATION":g.ElevationOutput(mapdata.elevation()),
            "TOTALTIME":g.TimeOutput(mapdata.time()),
            "TOTALDISTANCE":g.DistanceOutput(mapdata.distance())
            }

    # Navigate through our choices and call the associated method
    for choice in choices:
        try:
            choiceMap[choice].string()
        except:
            print("INPUT ERROR: {}".format(choice))

def get_input() -> list:
    input_data = []
    #Input number of following lines
    values = int(input())
    for i in range(values):
        input_data.append(input())
    return input_data

        

def main():
    ''' Main entry point of the program '''
    locations = get_input()
    outputs = get_input()
    mp = mapquest.Mapquest("3gSWTeiHeAf95mbEJ1f4djz9NHgdDzK7")
    mp.set_locations(locations)
    data = mp.load()

    # Ensure data actually contains data
    if data:
        output(outputs,data)
        print("Directions Courtesy of MapQuest; Map Data Copyright OpenStreetMap Contributors.")
     
    
if __name__ == '__main__':
    main()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def test_data_set():
    ''' Runs through database of addresses as test data '''
    with open('randAddresses.json') as df:
        dataset = json.load(df)
        dataset = dataset["addresses"]

    test_data = []
    random.seed()
    numLocations = random.randint(2,10)
    for i in range(numLocations):
        test_data.append(dataset[random.randint(1,len(dataset))]["id"])
    debug(test_data)

def debug(loc:list):
    mp = mapquest.Mapquest("3gSWTeiHeAf95mbEJ1f4djz9NHgdDzK7")
    mp.set_locations(loc)
    mapdata = mp.load()

    print("Narrative: {}".format(mapdata.narrative()))
    print("LatLng: {}".format(mapdata.latlng()))
    print("Time: {}".format(mapdata.time()))
    print("Distance: {}".format(mapdata.distance()))
    print("Elevation: {}".format(mapdata.elevation()))
