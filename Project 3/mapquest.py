# Anthony Lam
# 77341985

import json
from urllib import parse,request
from collections import OrderedDict


class Mapquest():

    ELEVATION_REQUEST = "http://open.mapquestapi.com/elevation/v1/profile"
    ROUTE_REQUEST = "http://open.mapquestapi.com/directions/v2/route"

    def __init__(self,api_key:str,unit:str='m') -> None:
        self.api_key = api_key
        self.unit = unit

    def set_locations(self,locations:list) -> None:
        self.loc_from = locations[0]
        self.loc_to = locations[1:]

    def load(self):
        ''' Loads route and elevation data '''
        mapdata = MapData(self._load_route())
        mapdata.set_profile(self._load_elevation(mapdata))

        if mapdata.check_status():
            return mapdata
        else:
            return None


    def _load_elevation(self,mapdata) -> {}:
        ''' Get elevation data from mapquest '''
        latlng = mapdata.latlng()
        elevation = []
        for ll in latlng:
            col = "{},{}".format(ll[0],ll[1])
            query = {'key':self.api_key,
                     'latLngCollection':col}
            url = self._build_request(self.ELEVATION_REQUEST,query)
            elevation.append(self._request(url))

        return elevation


    def _load_route(self) -> {}:
        ''' Get route data from mapquest '''
        query = {'key':self.api_key,
                 'from':self.loc_from,
                 'unit':self.unit}
        url = self._build_request(self.ROUTE_REQUEST,query,self.loc_to)
        route = self._request(url)
        return route


    def _request(self,url:str) -> str:
        # Make an API call
        response = request.urlopen(url)
        print(url)
        data = self._parse_response(response.read().decode())
        return data


    def _parse_response(self,response:str) -> dict:
        try:
            response = json.loads(response,object_pairs_hook=OrderedDict)
        except Exception as e:
            print(e)
        return response


    def _build_request(self,base_request:str,query:dict,to=[]) -> str:
        encoded_query = parse.urlencode(query)
        to_query = ""
        for item in to:
            to_query = to_query + "&" + parse.urlencode({"to":item})
        url = base_request + "?"+ encoded_query + "&" + to_query
        return url

class MapData():

    def __init__(self,route:{}=None,profile:{}=None):
        self._route = route
        self._profile = profile

    def set_route(self,route:{}):
        self._route = route

    def set_profile(self,profile:{}):
        self._profile = profile

    def latlng(self) -> [(float,float)]:
        ''' Load location as tuple pairs in (lat,long)'''
        locations = []
        for item in self._route["route"]["locations"]:
            lat = item["latLng"]["lat"]
            lng = item["latLng"]["lng"]
            locations.append((lat,lng))
        return locations

    def time(self,unit:str='s') -> int:
        ''' Loads elapsed time in seconds '''
        time = self._route["route"]["time"]

        if unit == 's':
            return time
        elif unit == 'm':
            return (time/60)
        elif unit == 'h':
            return (time/360)

    def distance(self) -> (float,str):
        ''' Loads total distance and specified units '''
        distance = self._route["route"]["distance"]
        unit = self._route["route"]["options"]["unit"]
        if unit == "M":
            return (distance,"miles")
        elif unit == "K":
            return (distance,"kilometers")
        
    def elevation(self) -> [int]:
        ''' Loads a list of elevation profiles ''' 
        elevation = []
        for profile in self._profile:
            for item in profile["elevationProfile"]:
                elevation.append(item["height"])
        return elevation

    def narrative(self) -> [str]:
        ''' Loads a list of directions '''
        narrative = []
        for item in self._route["route"]["legs"]:
            for maneuvers in item["maneuvers"]:
                narrative.append(maneuvers["narrative"])
        return narrative

    def check_status(self) -> bool:
        ''' Check connection status to Mapquest '''
        class RouteError():
            pass
        class StdError():
            pass
        rteErrors = [400]
        stdErrors = [range(600,700)]
        try:
            status = self._route["info"]["statuscode"]
            if status != 0:
                if status in rteErrors:
                    raise RouteError
                else:
                    raise StdError
            return True
        except RouteError:
            print()
            print("NO ROUTE FOUND")
            return False
        except StdError:
            print()
            print("MAPQUEST ERROR")
            return False
