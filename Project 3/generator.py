# Anthony Lam
# 77341985

import mapquest

class LatlngOutput():

    def __init__(self,latlng:list):
        self._latlng = latlng

    def string(self):
        ''' Converts to string output '''
        print("LATLONGS")
        for item in self._latlng:
            out = ""
            if item[0] > 0:
                out = "{0:.2f}N".format(item[0])
            else:
                out = "{0:.2f}S".format(item[0])
            
            print(out,end=" ")

            if item[1] > 0:
                out = "{0:.2f}W".format(item[1])
            else:
                out = "{0:.2f}E".format(item[1])
            print(out)
        print()


class DirectionOutput():

    def __init__(self,narrative:list):
        self._narrative = narrative

    def string(self):
        ''' Converts to string output '''
        print("DIRECTIONS")
        for item in self._narrative:
            print(item)
        print()

class ElevationOutput():

    def __init__(self,elevation:list):
        self._elevation = elevation

    def string(self):
        ''' Converts to string output '''
        print("ELEVATIONS")
        for item in self._elevation:
            print(item)
        print()

class DistanceOutput():

    def __init__(self,distance:tuple):
        self._distance = distance

    def string(self):
        ''' Converts to string output '''
        print("TOTAL DISTANCE: {} {}".format(int(self._distance[0]),self._distance[1]))
        print()

class TimeOutput():

    def __init__(self,time:int):
        self._time = time

    def string(self):
        ''' Converts to string output '''
        print("TOTAL TIME: {}".format(self._time))
        print()
            

